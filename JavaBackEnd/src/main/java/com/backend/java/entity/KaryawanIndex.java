package com.backend.java.entity;

import java.sql.Date;

public class KaryawanIndex {
	private int no;
	private String name;
	private Date birthDate;
	private String jabatan;
	private int nip;
	private int jenisKelamin;

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getjabatan() {
		return jabatan;
	}

	public void setJabatan(String jabatan) {
		this.jabatan = jabatan;
	}

	public int getNip() {
		return nip;
	}

	public void setNip(int nip) {
		this.nip = nip;
	}

	public int getJenisKelamin() {
		return jenisKelamin;
	}

	public void setJenisKelamin(int jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public KaryawanIndex(int no, String name, Date birthDate, String jabatan, int nip, int jenisKelamin) {
		super();
		this.no = no;
		this.name = name;
		this.birthDate = birthDate;
		this.jabatan = jabatan;
		this.nip = nip;
		this.jenisKelamin = jenisKelamin;
	}

	public KaryawanIndex() {
		// TODO Auto-generated constructor stub
	}

}
