package com.backend.java.entity;

import java.time.LocalDate;

public class Employee {
	private int id;
	private String name;
	private LocalDate birthDate;
	private int positionId;
	private int idNumber;
	private int gender;
	private int isDelete;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public int getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(int idNumber) {
		this.idNumber = idNumber;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}

	public Employee(int id, String name, LocalDate birthDate, int positionId, int idNumber, int gender, int isDelete) {
		super();
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.positionId = positionId;
		this.idNumber = idNumber;
		this.gender = gender;
		this.isDelete = isDelete;
	}

}
