package com.backend.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backend.java.entity.KaryawanIndex;
import com.backend.java.model.TambahDataKaryawan;
import com.backend.java.service.AllService;

@RestController
@RequestMapping("/promise")
public class ApplicationController {

	@Autowired
	AllService allService;

	@GetMapping(value = "/karyawanindex")
	public List<KaryawanIndex> getAll() {		
		List<KaryawanIndex> result = allService.findAll();
		return result;

	}

	@PostMapping(value = "/karyawancreate")
	public void createData(@RequestBody TambahDataKaryawan emp) {
		allService.insertData(emp);

	}
	
	@PostMapping(value = "/karyawaneditadd")
	public void updateData(@RequestBody TambahDataKaryawan emp) {
		allService.updateData(emp);

	}

	@PutMapping(value = "/karyawaneditadd")
	public void updateDataExecute(@RequestBody TambahDataKaryawan emp) {
		allService.updateDataExecute(emp);

	}

	@DeleteMapping(value = "/karyawandelete")
	public void deleteEmployee(@RequestBody TambahDataKaryawan emp) {
		allService.deleteData(emp);

	}

}
