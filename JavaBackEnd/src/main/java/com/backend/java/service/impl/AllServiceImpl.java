package com.backend.java.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.backend.java.entity.Employee;
import com.backend.java.entity.KaryawanIndex;
import com.backend.java.entity.Position;
import com.backend.java.model.TambahDataKaryawan;
import com.backend.java.service.AllService;

@Repository
public class AllServiceImpl implements AllService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<KaryawanIndex> findAll() {
		return jdbcTemplate.query("select TE.ID as no, TE.NAME as name, TE.BIRTHDATE as birthdate, \r\n"
				+ "TP.NAME as jabatan, TE.ID_NUMBER as nip, TE.GENDER as jeniskelamin  from T1_POSITION TP INNER JOIN T2_EMPLOYEE TE \r\n"
				+ "ON TP.ID = TE.POSITION_ID ORDER BY TE.ID ASC",
				(rs, rowNum) -> new KaryawanIndex(rs.getInt("no"), rs.getString("name"), rs.getDate("birthdate"),
						rs.getString("jabatan"), rs.getInt("nip"), rs.getInt("jeniskelamin")));
	}

	@Override
	public int insertData(TambahDataKaryawan tambahDataKaryawan) {
//		KaryawanIndex karyawanIndex = new KaryawanIndex();
//		Position position = jdbcTemplate.queryForObject("select * from T1_POSITION where name = ?",
//				new Object[] { tambahDataKaryawan.getJabatan() }, (rs, rowNum) -> new Position(rs.getLong("id"),
//						rs.getString("code"), rs.getString("name"), rs.getInt("isdelete")));
//		Employee employee = jdbcTemplate.queryForObject("select * from T2_EMPLOYEE where position_id = ?",
//				new Object[] { position.getId()}, (rs, rowNum) -> new Employee(rs.getLong("id"),
//						rs.getString("code"), rs.getString("name"), rs.getInt("isdelete")));

		return 1;
	}

	@Override
	public List<KaryawanIndex> updateData(TambahDataKaryawan emp) {
		return jdbcTemplate.query("select * from books where id = ?",
				(rs, rowNum) -> new KaryawanIndex(rs.getInt("id"), rs.getString("name"), rs.getDate("birthDate"),
						rs.getString("jabatan"), rs.getInt("nip"), rs.getInt("jenisKelamin")));
	}

	@Override
	public int deleteData(TambahDataKaryawan emp) {
		return jdbcTemplate.update("delete books where id = ?", emp.getId());
	}

	@Override
	public int updateDataExecute(TambahDataKaryawan emp) {
		return jdbcTemplate.update("update books set price = ? where id = ?", emp.getBirthDate(), emp.getId());
	}
}
