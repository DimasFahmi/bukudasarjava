
public class While {

	public static void main(String[] args) {
		int angka_while = 15;
		int angka = 15;
		int pembagi = 6;
		int hasil = 0;
		int sisa = 0;
		int perulangan = 0;

		while (angka > pembagi) {
			perulangan = perulangan + 1;
			System.out.println("Perulangan ke " + perulangan);
			System.out.println("Angka masih " + angka);
			hasil = hasil + 1;
			angka = angka - pembagi;
			System.out.println("Angka setelah dikurangi " + pembagi + " Adalah " + angka);
			System.out.println();
		}

		System.out.println("Angka sudah dibawah " + pembagi);
		System.out.println("Sehingga Hasilnya adalah ");
		System.out.println("Hasil " + angka_while + " Dibagi " + pembagi + " Adalah " + hasil + " Sisa " + angka);
	}
}
